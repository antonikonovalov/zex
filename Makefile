
all: install test run

run:
	$$GOPATH/bin/forego start

install:
	go get ./...
	go get github.com/ddollar/forego
	GOBIN=`pwd`/bin go install ./cmd/...
test:
	go test ./...
