package main

import (
	"flag"
	"fmt"
	"net"

	"gitlab.com/antonikonovalov/zex/proto/zex"
	"gitlab.com/antonikonovalov/zex/server"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
)

var (
	port = flag.Int("port", 10000, "The server port")
)

func main() {
	flag.Parse()

	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)

	zexServer := server.New()
	zex.RegisterZexServer(grpcServer, zexServer)
	grpcServer.Serve(listener)
}
