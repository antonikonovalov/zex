package main

import (
	"flag"
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	"google.golang.org/grpc/grpclog"
	"io"

	"gitlab.com/antonikonovalov/zex/proto/zex"

	"github.com/golang/protobuf/proto"
	"gitlab.com/antonikonovalov/zex/examples/services/a"
)

var (
	zexServerAddr = flag.String("zex", "127.0.0.1:10000", "Zex server in the format of host:port")
)

func main() {
	flag.Parse()
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	conn, err := grpc.Dial(*zexServerAddr, opts...)
	if err != nil {
		grpclog.Fatalf("fail to dial: %v", err)
	}
	defer conn.Close()

	client := zex.NewZexClient(conn)

	grpclog.Printf("Start send pipeline task to in Zex")
	stream, err := client.Pipeline(context.Background())
	if err != nil {
		grpclog.Fatalf("%v.Pipeline(_) = _, %v", client, err)
	}

	// тут мы шлем реальное тело
	var (
		req1 = &a.Req{Name: "AloxaA"}
		req2 = &a.Req{Name: "AloxaB"}
		req3 = &a.Req{Name: "AloxaC"}
	)

	body1, _ := proto.Marshal(req1)
	body2, _ := proto.Marshal(req2)
	body3, _ := proto.Marshal(req3)

	// три раза!
	if err := stream.Send(&zex.Cmd{zex.CmdType_INVOKE, "/a.A/CallA", body1}); err != nil {
		grpclog.Fatalf("%v.Send(%v) = %v", stream, "/a.A/CallA", err)
	}
	if err := stream.Send(&zex.Cmd{zex.CmdType_INVOKE, "/a.A/CallB", body2}); err != nil {
		grpclog.Fatalf("%v.Send(%v) = %v", stream, "/a.A/CallB", err)
	}
	if err := stream.Send(&zex.Cmd{zex.CmdType_INVOKE, "/a.A/CallC", body3}); err != nil {
		grpclog.Fatalf("%v.Send(%v) = %v", stream, "/a.A/CallC", err)
	}

	pid, err := stream.CloseAndRecv()
	if err == io.EOF {
		grpclog.Printf("close stream")
	} else if err != nil {
		grpclog.Fatalf("%v.CloseAndRecv() got error %v, want %v", stream, err, nil)
		return
	}
	grpclog.Printf("Pipeline close: %v", pid)
}
